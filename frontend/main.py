import tkinter.messagebox as tk_message
import webbrowser
import os
from tkinter import *
from tkinter import ttk

from backend.main import (
    plotter,  # Will import to torp/main
    pause_plotting,
    continue_plotting,
    clear_plotting,
    connect_backend,
    disconnect
)


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


ICON_BITMAP = resource_path("images\\aju.ico")
ICON_CONNECTION = resource_path("images\\connection.png")
ICON_CANCEL = resource_path("images\\cancel.png")
ICON_REFRESH = resource_path("images\\refresh1.png")
ICON_SCREENSHOT = resource_path("images\\screenshot.png")

YOUTUBE_LINK = "https://www.youtube.com/watch?v=bkLCS9-J4z4"
filename = ""


def doNothing():
    tk_message.showinfo("TORP", "Suck it, you're trapped. Enjoy!")


def goBackend(com, baud, window):
    result = connect_backend(com, baud)

    if result is True:
        window.destroy()

    else:
        tk_message.showinfo("Connection Error", f"Error message:\n{result}")


def whichCom():

    coms = ["COM1", "COM2", "COM3", "COM4", "COM5"]
    bauds = [1200, 2400, 4800, 9600, 57600, 115200]

    connectionWindow = Toplevel()
    connectionWindow.iconbitmap(ICON_BITMAP)
    connectionWindow.geometry("200x150+800+350")
    connectionWindow.resizable(0, 0)

    selectBaud = ttk.Combobox(connectionWindow, value=bauds, font=15)
    selectCom = ttk.Combobox(connectionWindow, value=coms, font=15)

    okButton = Button(
        connectionWindow,
        text="Ok",
        font=15,
        command=lambda: goBackend(
            coms[selectCom.current()], bauds[selectBaud.current()], connectionWindow
        ),
    )
    okButton.place(relx=0.1, rely=0.7, relheight=0.15, relwidth=0.35)

    closeButton = Button(
        connectionWindow, text="Close", font=15, command=connectionWindow.destroy
    )
    closeButton.place(relx=0.55, rely=0.7, relheight=0.15, relwidth=0.35)

    valik = StringVar()
    welConnectionLabel = Label(connectionWindow, text="Connecting..", font=15).pack(
        side=TOP
    )

    selectCom.current(2)

    selectCom.place(relx=0.5, rely=0.2, relheight=0.15, relwidth=0.4)

    serialLabel = Label(connectionWindow, text="Serial Port", font=15, anchor=W)
    serialLabel.place(relx=0.25, rely=0.2, relwidth=0.4, relheight=0.15, anchor="n")

    myLabel = Label(status, text=selectCom.get())
    myLabel.grid(row=0, column=1)

    selectBaud.current(4)

    selectBaud.place(relx=0.5, rely=0.4, relheight=0.15, relwidth=0.4)

    baudLabel = Label(connectionWindow, text="Baud Rate", font=15, anchor=W)
    baudLabel.place(relx=0.25, rely=0.4, relwidth=0.4, relheight=0.15, anchor="n")
    connectionWindow.focus_set()
    connectionWindow.grab_set()


class DropMenu:
    def __init__(self, master):
        self.menu = Menu(master)
        master.config(menu=self.menu)

        self.subMenu = Menu(self.menu)
        self.menu.add_cascade(label="File", menu=self.subMenu)
        self.subMenu.add_command(label="Connect", command=whichCom)
        self.subMenu.add_command(label="Disconnect", command=disconnect)
        self.subMenu.add_separator()
        self.subMenu.add_command(label="Exit", command=master.quit)

        self.viewMenu = Menu(self.menu)
        self.menu.add_cascade(label="View", menu=self.viewMenu)

        self.helpMenu = Menu(self.menu)
        self.menu.add_cascade(label="Help", menu=self.helpMenu)

        new = 1
        url = YOUTUBE_LINK

        def openweb():
            webbrowser.open(url, new=new)

        self.helpMenu.add_command(label="About TORP..", command=openweb)


class MainFrame:
    def __init__(self, master):
        self.upperFrame = Frame(master, bd=4, relief=SUNKEN)
        self.upperFrame.place(
            relx=0.62, rely=0.175, relwidth=0.65, relheight=0.7, anchor="n"
        )

        self.menuFrame = Label(master, bg="GREY", bd=1, relief=SUNKEN, anchor=W)
        self.menuFrame.place(
            relx=0.05, rely=0, relwidth=0.35, relheight=0.96, anchor="n"
        )

        self.startButton = Button(
            self.menuFrame, text="Start", font=20, command=continue_plotting
        )
        self.startButton.place(relx=0.6, rely=0.05, relheight=0.55, relwidth=0.4)

        self.exitButton = Button(
            self.menuFrame, text="Stop & Exit", font=20, command=master.quit
        )
        self.exitButton.place(relx=0.3, rely=0.75, relheight=0.25, relwidth=0.7)

        self.stopButton = Button(
            self.menuFrame, text="Stop", font=20, command=pause_plotting
        )
        self.stopButton.place(relx=0.35, rely=0.05, relheight=0.55, relwidth=0.25)

        self.clearButton = Button(
            self.menuFrame, text="Clear Data", font=20, command=clear_plotting
        )
        self.clearButton.place(relx=0.3, rely=0.6, relheight=0.15, relwidth=0.7)


# Navbar
root = Tk()
dropMenu = DropMenu(root)
mainFrame = MainFrame(root)
root.title("TORP")
root.iconbitmap(ICON_BITMAP)
root.geometry("600x450+600+200")
root.resizable(1, 0)

# Toolbar
toolbar = Frame(root, bg="white", relief="ra")

# Midagi on PhotoImage'i ja .png failidega omamoodi
# Vist PIL library vaja kasutada
# https://stackoverflow.com/questions/38602594/how-do-i-fix-the-image-pyimage10-doesnt-exist-error-and-why-does-it-happen
# master=root parandas praegust probleemi
image1 = PhotoImage(file=ICON_CONNECTION, master=root)
image2 = PhotoImage(file=ICON_CANCEL, master=root)
image3 = PhotoImage(file=ICON_REFRESH, master=root)
image4 = PhotoImage(file=ICON_SCREENSHOT, master=root)

insert1 = Button(toolbar, image=image1, relief=RAISED, command=whichCom)
insert1.pack(side=LEFT, padx=2, pady=2)

insert2 = Button(toolbar, image=image2, relief=RAISED, command=doNothing)
insert2.pack(side=LEFT, padx=2, pady=2)

insert3 = Button(toolbar, image=image3, relief=RAISED, command=doNothing)
insert3.pack(side=LEFT, padx=2, pady=2)

insert4 = Button(toolbar, image=image4, relief=RAISED, command=doNothing)
insert4.pack(side=LEFT, padx=2, pady=2)

toolbar.pack(side=TOP, fill=X)

# Statusbar
status = Label(root, text="Port..", bd=0.5, relief=SUNKEN, anchor=W)
status.pack(side=BOTTOM, fill=X)
# signalQuality = Label(status, text="Quality: 100%").place(relx=0.87, rely=0)
# See pmst nagu töötab, aga väärtusi ei uuenda (ehk püsib 0.0)
# Pealegi annab ühe väärtuse minutis, mis on suht harva
# Pigem äkki las see jääda
# signalQuality = Label(status, text=f"Quality: {get_signal()}").place(relx=0.87, rely=0)

# Nuppe

# button_quit = Button(root, text="Quit Program", command = root.quit)
# button_quit.pack(side=BOTTOM)
# frame

# Message
# tk_message.showinfo("TORP", "Shit is incomplete. Enjoy!")

whichCom()
# Comment out to run in MAIN
root.mainloop()
