from threading import Thread
from backend.mindPlayReader import MyndPlayReader
from backend.Plotter import Plotter

T_READER = Thread
T_PLOTTER = Thread
mpr = MyndPlayReader()
plotter = Plotter()
threads_dict = {
        "reader": {
            "target": mpr.run,
            "args": ()
        },
        "plotter": {
            "target": plotter.run_time_plotting,
            "args": (mpr,)
        }
    }


def connect_backend(port, baud):
    """
    Initiate background procedure via selected COM port
    """
    if not mpr.ser.is_open:
        result = mpr.connect_serial(port, baud)

        if result is True:
            threads_dict.get("plotter").update(dict(args=(mpr,)))
            start_all_threads()

            return True

        return result

    else:
        return True


def start_thread(option):
    """
    Create a thread with specified target function
    """

    assert option == "reader" or option == "plotter", f"Invalid option {option}"
    global T_READER, T_PLOTTER

    thread_obj = threads_dict[option]

    target = thread_obj.get("target")
    args = thread_obj.get("args")

    thread = Thread(target=target, args=args)
    thread.daemon = True
    thread.start()

    if option == "reader":
        T_READER = thread
    else:
        T_PLOTTER = thread


def start_all_threads():
    """
    Start all threads
    """
    start_thread("reader")
    start_thread("plotter")


def pause_plotting():
    """
    Pause plotting the graph
    """
    plotter.set_values(mpr.x_axis, mpr.y_axis)
    plotter.pause_plot()


def continue_plotting():
    """
    Continue plotting the graph
    """
    plotter.continue_plot()


def clear_plotting():
    """
    Clears the plot graph
    """
    if not plotter.pause:
        plotter.pause_plot()
        plotter.set_values(mpr.x_axis, mpr.y_axis)
        mpr.clear_all()
        plotter.continue_plot()

    else:
        mpr.clear_all()
        plotter.set_values([], [])


def get_signal():
    """
    Get last known signal value from headband
    """
    return mpr.signal


def disconnect():
    mpr.close()
    plotter.refresh()
    mpr.refresh()

    threads_dict.get("reader").update(dict(target=mpr.run))
    threads_dict.get("plotter").update(dict(target=plotter.run_time_plotting))
