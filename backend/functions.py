from itertools import count

COUNTER = count()


def check_indexes(stream):
    """
    In case of more that 2 \xaa values in a row
    Keep only the last pair
    """
    return_stream = stream.copy()

    for index, val in enumerate(zip(stream[:-1], [i - 1 for i in stream[1:]])):
        if val[0] == val[1]:
            del return_stream[index]

    return return_stream


def get_index():
    """
    Return next y value as index
    """
    return float(next(COUNTER))


def reset_index():
    """
    Resets counter value if graph is cleared
    """
    global COUNTER
    COUNTER = count()
