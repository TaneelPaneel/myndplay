import time
import matplotlib.pyplot as pyplt
from itertools import count
from collections import deque
from matplotlib.animation import FuncAnimation

WINDOW_LENGHT = 128
FRAMES = count()


class Plotter:
    """
    Signal plotting class to visualize data received from reader
    """

    # https://stackoverflow.com/a/41134818

    def __init__(self):
        self.figure = pyplt.figure()

        # Anything over 1000 is usually an error
        self.ax = pyplt.axes(xlim=(-WINDOW_LENGHT, 0), ylim=(-1000, 1000))
        (self.line,) = self.ax.plot([], [], "-r")  # Initialize signal values to None
        self.xdata, self.ydata = [], []
        self.ani = FuncAnimation
        self.pause = False

    def run_time_plotting(self, reader):
        """
        Initiate plotting live data.
        """
        self.continue_plot()

        def init_animation():
            """
            Initiate the graph axis values
            Required for more efficient plotting
            """

            self.line.set_data([], [])
            return (self.line,)

        def animate(i):
            """
            Will update every interval x and y values in plot
            """
            if not self.pause:
                self.line.set_data(reader.x_axis, reader.y_axis)
                pyplt.xlim(reader.x_axis[-1] - WINDOW_LENGHT, reader.x_axis[-1])

            else:
                self.line.set_data(self.xdata, self.ydata)

            return (self.line,)

        # Wait for first data packets
        while reader.y_axis[0] == 0.0 and reader.y_axis[-1] == 0.0:  # Whole list of 0.0
            time.sleep(0.05)

        self.ani = FuncAnimation(
            self.figure,
            animate,
            init_func=init_animation,
            interval=34,  # in ms - will make ~30 FPS
            blit=True  # == True to use blitting - drops CPU usage ~ in half
        )

    def set_values(self, _x_data, _y_data):
        """
        Sets x and y values
        Must be of list or deque type
        """
        if (type(_x_data) in (list, deque)) and (type(_y_data) in (list, deque)):
            self.xdata = _x_data.copy()
            self.ydata = _y_data.copy()

    def pause_plot(self):
        self.pause = True

    def continue_plot(self):
        self.pause = False

    def refresh(self):
        """
        Refreshes the Plotter class elements
        Basically re-initialize
        """
        global FRAMES
        FRAMES = count()
        self.ani.event_source.stop()
        (self.line,) = self.ax.plot([], [], "-r")
        self.ax.set_xlim(-WINDOW_LENGHT, 0)
        self.ax.set_ylim(-1000, 1000)
        self.xdata, self.ydata = [], []
        self.pause = False
        self.figure.canvas.draw()

