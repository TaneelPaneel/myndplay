import re
import time
import serial
from backend.functions import *
from collections import deque

WINDOW_LENGHT = 128

# https://pyserial.readthedocs.io/


class MyndPlayReader:
    """
    The MyndPlay headband signal reader and data processor
    """

    window_length = WINDOW_LENGHT

    def __init__(self):
        self.total = b""
        self.max_byte_value = 255
        self.x55 = 85  # 0x55
        self.x80 = 128  # 0x80
        self.ser = serial.Serial()
        self.packet = deque()
        self.closed = False

        # Don't use
        # x_axis = y_axis = []
        # This will bind same list object to both variables
        self.x_axis = self.clear_deque
        self.y_axis = self.clear_deque
        self.signal_quality = self.clear_deque

        # Python does not have switch-case object
        # So we need to make our own
        self.switcher = {
            2: self.get_signal_quality,
            6: self.get_singlebyte_raw_value,
            128: self.get_multibyte_raw_value
        }

    def connect_serial(self, port="COM3", baud=56700):
        """
        Establish a connection with
        the MyndBand headband
        """

        self.ser.baudrate = baud
        self.ser.port = port

        try:
            self.ser.open()
        except serial.serialutil.SerialException or ValueError as e:
            # SerialException: Device can not be configured
            # ValueError: Parameter out of range
            return e

        print(f"Serial connection is {'OPEN' if self.ser.is_open else 'CLOSED'}")

        self.closed = False
        return True

    def read_serial(self):
        """
        Reading byte values from serial connection
        and converting them into human-readable values
        """
        # If no bytes in input buffer
        while not self.ser.in_waiting:
            time.sleep(0.01)  # Sleep 10ms

        # Read bytes in input buffer
        raw_buffer = self.ser.read(self.ser.in_waiting)
        if len(raw_buffer) >= 2048:
            print("Max buffer lenght. Some data might have been lost")
        raw_buffer = self.total + raw_buffer

        # Find all indexes of 2x "\xaa" bytes
        aa_indx = [i.start() for i in re.finditer(b"\xaa\xaa", raw_buffer)]

        # Validate indexes
        aa_indx = check_indexes(aa_indx)

        # In case of reading only half a packet
        if len(aa_indx) < 2:
            self.total = raw_buffer
            # TODO When reading only half a packet do we still continue or go to next loop?

        # Save the left over bytes
        self.total = raw_buffer[aa_indx[-1] : -1]

        processed_buffer = self.process_serial(aa_indx, raw_buffer)

        # Cut the leftover
        # buffer = raw_buffer[:aa_indx[-1]]

        return processed_buffer, raw_buffer

    def process_serial(self, aa_indx, stream):
        """
        :param aa_indx: List of all 0xAA locations in stream buffer
        :param stream: Whole buffer read
        Processing the bytestream values gathered from the headband
        for readable data extraction
        """
        extracted_packets = []
        for i in range(len(aa_indx) - 1):

            # At least 2 bytes between
            if aa_indx[i] != aa_indx[i + 1] - 2:

                # Verify and get extracted data
                tmp_buffer, checksum = self.verify_packet(stream[aa_indx[i] : aa_indx[i + 1]])
                # If checksum matches
                if checksum:
                    extracted_packets = self.parser(tmp_buffer)

        return extracted_packets

    def verify_packet(self, packet):
        """
        :param packet: Slice of data (with end and beginning) to be verified
        Verify the integrity of a packet in stream buffer
        Has some data been lost or is packet OK
        """
        # Actual data bytes
        actual_data = packet[3:-1]
        # Data checksum
        payload_sum = sum(actual_data) & self.max_byte_value

        # Packet checksum
        # Last byte is raw checksum
        checksum = packet[-1] & self.max_byte_value
        checksum = self.max_byte_value - checksum  # Calculate 1's inverse

        # Return data bytes and checksum match
        return actual_data, checksum == payload_sum

    def parser(self, buffer):
        """
        :param buffer: Verified packet from actual data will be extracted from
        Gather real measurable data from a single packet
        """
        packet_length = len(buffer)
        index = 0
        result = []

        while index < packet_length:
            # EXCODE level for current buffer
            # EXCODE level = number of EXCODE bytes before CODE byte
            # EXCODE byte = 0x55 - CODE is never 0x55
            excode_lvl = 0
            while buffer[index] == self.x55:
                excode_lvl += 1
                index += 1

            # CODE byte value for this buffer
            code_byte = buffer[index]
            index += 1

            # Calculating number of actual data bytes
            # CODE < 0x80 are single-byte values
            # CODE >= 0x80 are multi-byte values
            # For multi-byte values, the lenght of the multi-byte value immediately follows CODE
            if code_byte < self.x80:
                # Single byte
                result.append(buffer[index - 1 : index + 1])  # Yes - a list of lists

            else:
                # Multi byte
                # Need to add +1 because of python list indexing
                # List indexing will reach index element and stop BEFORE it
                # while elem[index] > index ... also need the last element
                value_length = buffer[index] + 1

                if (value_length + index) <= packet_length:
                    result.append(buffer[index - 1 : index + value_length])

                index += value_length + 1

        # Return parsed data
        return result

    def run(self):
        """
        After port opening start reading data
        from serial connection
        """
        while not self.closed:
            data_packets, raw_bytes = self.read_serial()
            while not data_packets:
                data_packets, raw_bytes = self.read_serial()

            for packet in data_packets:
                self.packet = deque(packet)
                func = self.switcher.get(self.packet[0])

                if func:  # Or None
                    func()

                else:
                    # If haven't created a switch func for current byte value yet
                    print(f"None || {packet[0]}")
        else:
            print("Reader already closed")

    def get_multibyte_raw_value(self):
        """
        Get raw wave 16 bit value
        """
        signal_value = self.packet[2] * 256 + self.packet[3]
        if signal_value > 32767:
            signal_value = signal_value - 65536

        self.x_axis.popleft()
        self.x_axis.append(get_index())

        self.y_axis.popleft()
        self.y_axis.append(signal_value)

    def get_singlebyte_raw_value(self):
        """
        Get raw wave 8 bit value
        """
        self.x_axis.popleft()
        self.x_axis.append(get_index())

        self.y_axis.popleft()
        self.y_axis.append(self.packet[3])

    def get_signal_quality(self):
        """
        Get the signal quality value
        """
        self.signal_quality.popleft()
        self.signal_quality.append(self.packet[1])

    def clear_all(self):
        """
        Reset all X, Y values to 0
        """
        self.x_axis = self.clear_deque
        self.y_axis = self.clear_deque
        reset_index()

    def close(self):
        """
        Closes connection and stops process
        """
        if not self.closed:
            self.closed = True
            time.sleep(0.2)
            self.ser.close()

    def refresh(self):
        self.clear_all()
        self.closed = False
        self.signal_quality = self.clear_deque

    @property
    def clear_deque(self):
        return deque([0.0] * self.window_length)

    @property
    def signal(self):
        return self.signal_quality[-1]
