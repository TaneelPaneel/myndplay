# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

added_files = [("images\\*", "images")]

a = Analysis(['torp.py'],
             pathex=['C:\\Users\\Tanel\\Documents\\IAS1420\\myndplay\\venv\\Lib\\site-packages', 'C:\\Users\\Tanel\\Documents\\IAS1420\\myndplay'],
             binaries=[],
             datas=added_files,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='TORP',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
          icon="thinking.ico" )
