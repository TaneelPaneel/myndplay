import frontend.main as frend
from tkinter import BOTTOM, BOTH
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

# Graph to tkinter window:
# https://datatofish.com/matplotlib-charts-tkinter-gui/

# Use FigureCanvasTkAgg to plot
# https://stackoverflow.com/a/31453958

# Running animation
# https://stackoverflow.com/q/21197728

# First get the root Tk() and signal figure
root = frend.root
fig = frend.plotter.figure

# Create proper canvas and transfer graph to frame
# label = ttk.Label(root, text="Reader simulation").grid(column=0, row=0)

# First create canvas
canvas = FigureCanvasTkAgg(figure=fig, master=frend.mainFrame.upperFrame)
canvas.get_tk_widget().pack(side=BOTTOM, fill=BOTH, expand=True)
canvas.draw()

root.mainloop()
