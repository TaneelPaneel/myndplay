from vispy import app, scene
from matplotlib import pyplot
import numpy as np
import csv
import statistics
import bluetooth as bt
import pyqtgraph.Qt as qt
import pyqtgraph as pg
from threading import Thread
import time
SLEEP = 1/512


def use_vispy():
    # Ei tööta nii nagu peaks
    # Testing...
    canvas = scene.SceneCanvas(keys='interactive', show=True, size=(1024, 768))
    grid = canvas.central_widget.add_grid()
    view = grid.add_view(0, 1)
    view.camera = scene.MagnifyCamera(mag=1, size_factor=0.5, radius_ratio=0.6)

    # Add axes
    yax = scene.AxisWidget(orientation='left')
    yax.stretch = (0.05, 1)
    grid.add_widget(yax, 0, 0)
    yax.link_view(view)

    xax = scene.AxisWidget(orientation='bottom')
    xax.stretch = (1, 0.05)
    grid.add_widget(xax, 1, 1)
    xax.link_view(view)

    N = 4900
    M = 2000
    cols = int(N ** 0.5)
    view.camera.rect = (0, 0, cols, N / cols)

    lines = scene.ScrollingLines(n_lines=N, line_size=M, columns=cols, dx=0.8 / M,
                                 cell_size=(1, 8), parent=view.scene)
    lines.transform = scene.STTransform(scale=(1, 1 / 8.))

    def update(ev):
        m = 50
        data = np.random.normal(size=(N, m), scale=0.3)
        data[data > 1] += 4
        lines.roll_data(data)

    timer = app.Timer(connect=update, interval=0)
    timer.start()

    if __name__ == '__main__':
        import sys
        if sys.flags.interactive != 1:
            app.run()


def use_stream():
    # stream_file = open('C:/Users/Tanel/Desktop/NView/record.rawwave.txt', 'r')
    stream_file = open('test_files/test_stream', 'r')
    # Indeksi, mis sümoliseerib ajaühikut
    x = []  # Ajaväärtused
    y = []  # mõõteväärtused
    line_count = 0
    # See tagab, et kõik punktid joonistatakse ainult ühes aknas
    pyplot.ion()

    logline = read_from_stream(stream_file)
    # Samuti Option 1 osa
    fig = pyplot.figure().add_subplot(1, 1, 1)

    for y_value in logline:

        if line_count == 0:
            line_count += 1
            continue
            # print(f'Column names are {", ".join(line)}')
            # line_count +=1
        # else:

        # Option 1:
        # Kardan, et on aeglane, aga kuvab ühtse graafikuna
        # Tekitab pidevalt uue graafiku
        x.append(line_count)
        y.append(float(y_value))
        fig.clear()
        fig.plot(x, y)

        # Option 2:
        # Usun, et kiirem, kuna pidevalt lisab olemasolevale graafikule andmeid
        # Lisab punktid, mitte "signaali"
        # plot.scatter(x, y) == plot.plot(x, y, 'ro')
        # pyplot.scatter(line_count, float(y_value))

        # pyplot.draw()
        # pyplot.pause(SLEEP)
        line_count += 1

    pyplot.show(block=True)  # block=True lets the window stay open at the end of the animation.

    # Andmete jooksvaks kuvamiseks kasutada midagi sellist:
    # https://stackoverflow.com/questions/42024817/plotting-a-continuous-stream-of-data-with-matplotlib


def use_files_list():

    files_list = [
        "test_files/Test2/Test2.rawwave.csv",
        "test_files/Test2/Test2.filtered.csv",
    ]
    get_and_show_data(files_list)


def get_and_show_data(csv_files, default_lines=None):
    """
    :param csv_files: CSV failide list mida avada
            Väärtused peavad olema array kujul
    :param default_lines: Kui mitu rida CSV failist kuvada/sisse lugeda
            Vaikimisi väärtus: None (võtab kõik read)
    :return: X, Y väärtused
    """
    # Array of dictionaries
    # Tehakse faili kohta üks liige array-sse, milles sisalduvad dictionary kujul x, y väärtused
    data = []
    for file in csv_files:
        # Tühjad array'd väärtuste tagastamiseks
        x = []
        y = []
        # "with" avab .csv faili ning ka sulgeb selle ise automaatselt
        with open(file) as input_file:
            # Loen faili sisu eraldades erinevad väärtused
            csv_reader = csv.reader(input_file, delimiter=',')
            line_count = 0
            # Käin faili rida rea haavalt läbi
            for row in csv_reader:
                # Võtan vaid soovitud arvu ridu
                if line_count == default_lines:
                    break
                # Esimene rida on veergude nimetused
                elif line_count == 0:
                    print(f'Column names are {", ".join(row)}')
                    line_count += 1
                # Järgnevad juba väärtused
                else:
                    x.append(line_count)
                    y.append(row[1])
                    line_count += 1

            # Teen x, y väärtustest dictionary ja lisan selle 'data' array-sse,
            # et saaks mitu graafikut korraga kuvada
            temp = {'time': x, 'values': y}
            data.append(temp)

            # Kui mitu rida kokku oli
            print(f'Processed {line_count} lines')

    # Liigun edasi visualiseerima ilma main-ist läbi käimata
    visualize_data(data)


def visualize_data(values):
    """
    :param values: Array of dictionaries. Andmed, millel igal 'time' ja 'values' väärtustel (array kujul)
        on oma võti. Nii saab korraga kuvadda kui mitu graafikut.
    :return: Puudub
    """
    FRAMES_IN_SECOND = 500
    # Nii saab mitu graafikut erinevatesse akendesse korraga kuvatud
    i = 1
    for value in values:
        data, seconds = average_data(FRAMES_IN_SECOND / 2, value.get('time'), value.get('values'))
        pyplot.figure(i)
        pyplot.plot(seconds, data)
        i += 1
    pyplot.show()


def average_data(averaging_time, time=None, values=None):
    """
    :param averaging_time: Parameeter, mis näitab, mitu tulemust ennem kokku kogutakse, kui neist keskmine võetakse (
    Selle projekti puhul on 500 = 1 sekundiga) Väärtus peab olema integer
    :param time: Ajalised ühikud, millest võetakse keskmine
    :param values: Mõõtepunktid, millest võetakse kesmine
    :return: Tagastab sisse antud arrayd juba keskmistatud kujul
    """
    averaged_measurements = []
    temp = []
    j = 0
    for measurement in values:
        temp.append(float(measurement))
        if j == averaging_time:
            averaged_measurements.append(statistics.mean(temp))
            temp.clear()
            j = 0
        j += 1

    seconds = []
    i = 0
    for x in time:
        if i == averaging_time:
            seconds.append(x)
            i = 0
        i += 1
    return averaged_measurements, seconds


# Find all avaliable bluetooth connections
def find_connections(devices):
    # Initialize connection address
    connection_addr = None
    # List all found devices
    for device, name in devices:
        print("Name: {}, Address: {}".format(name, device))

        # To find headband address
        if "MyndBand" in name:
            print("Found MyndBand at address {}".format(device))
            connection_addr = str(device)

    return connection_addr


# Connecting via pyBluez using hardware address
def connect_headband(addr):
    # Creating client socket
    client_socket = bt.BluetoothSocket(bt.RFCOMM)

    # Connecting to device
    try:
        client_socket.connect((addr, 12))

    except Exception as e:
        print("Error: {}".format(e))
        raise

    # Testing recieved data
    try:
        data = client_socket.recv(1024)
        print("Recieved data: {}".format(data))

    except Exception as e:
        print("Error: {}".format(e))
        raise


def use_pyqtgraph():
    app = qt.QtGui.QApplication([])  # Initialize

    win = pg.GraphicsWindow(title="Signal from buffer")  # creates a window
    # creates empty space for the plot in the window
    p = win.addPlot(title="Realtime plot")
    # create an empty "plot" (a curve to plot)
    curve = p.plot()

    windowWidth = 512  # width of the window displaying the curve
    # create array that will contain the relevant time series
    Xm = np.linspace(0, 0, windowWidth)
    ptr = 0  # set first x position

    stream_file = open('test_files/test_stream/stream1.txt', 'r')

    # read line (single value) from the serial port
    value = read_from_stream(stream_file)
    for val in value:
        # shift data in the temporal mean 1 sample left
        Xm[:-1] = Xm[1:]
        try:
            # shift data in the temporal mean 1 sample left
            Xm[-1] = float(val)
        except ValueError:
            pass
        ptr += 1  # update x position for displaying the curve
        curve.setData(Xm)  # set the curve with this data
        curve.setPos(ptr, 0)  # set x position in the graph to 0
        qt.QtGui.QApplication.processEvents()  # process plot

    # END QtApp #
    pg.QtGui.QApplication.exec_()


def read_from_stream(file):
    i = 0
    full_file = []

    for i, line in enumerate(file):
        i += 1
        if i % 2 == 0:
            # Võtan vaid iga teise väärtuse
            full_file.append(line.rstrip().split(',')[1])

    return full_file

    # for row_data in full_file:
        # line = file.readline() # See readline() loeb terve rea, ma otsustasin seda mainis parsida ja ajaühiku lisada
        # if not line:
        #     time.sleep(SLEEP)
        #     continue

        # yield row_data


class Pyc2:
    """
    Eeeee...vist ei tööta päris hästi ikka...
    Olemuselt sama, mis use_pyqtgrahp(), kuid kasutanud selle jaoks class-i
    Nii saan läbi self -i mugavalt "globaalseid" muutujaid kasutada (ntks self.curve, self.frame...)
    """
    def __init__(self):
        self.app = qt.QtGui.QApplication([])  # you MUST do this once (initialize things)

        self.win = pg.GraphicsWindow(title="Signal from buffer")  # creates a window
        self.p = self.win.addPlot(title="Realtime plot")  # creates empty space for the plot in the window
        self.curve = self.p.plot()  # create an empty "plot" (a curve to plot)
        self.stop = False
        windowWidth = 512  # width of the window displaying the curve
        self.Xm = np.linspace(0, 0, windowWidth)  # create array that will contain the relevant time series
        self.ptr = 0  # set first x position

        stream_file = open('test_files/test_stream/stream1.txt', 'r')
        self.value = read_from_stream(stream_file)  # read line (single value) from the serial port

        # Tekitan threadi, mis hakkab Xm listi väätuseid uuendama
        # Xm [] ehk list, väärtustest, mida hetkel signaalil kuvatakse
        t = Thread(target=self.list_value)
        t.daemon = True
        t.start()

    def list_value(self):
        for val in self.value:
            self.Xm[:-1] = self.Xm[1:]  # shift data in the temporal mean 1 sample left
            # value = read_from_stream(stream_file)  # read line (single value) from the serial port
            try:
                self.Xm[-1] = float(val)  # vector containing the instantaneous values
            except ValueError:
                pass
            # Lasen magada, kuna muidu väärtused lähevad nihkesse teljestikuga signaali graafikul
            time.sleep(0.0000001)   # Muuda väärtust kui vaja - vist sõltub ka riistvarast

        # Siis, kui sisendfail on lõppenud
        self.end_f()

    def end_f(self):
        ### END QtApp ####
        self.stop = True
        pg.QtGui.QApplication.exec_()


def start_pyc_2():
    # Initsialiseerib classi
    a = Pyc2()
    while not a.stop:
        # Senikaua, kuni sisendfail pole otsas kuvan Xm[] listi graafikule
        a.curve.setData(a.Xm)
        a.curve.setPos(a.ptr, 0)

        # .processEvents() võtab suht kaua aega
        # Signaali väärtused uuenevad kiiremini, kui teljestik
        qt.QtGui.QApplication.processEvents()  # Process plot
        a.ptr += 1


# Kommenteerisin välja kuna videoga ei hakka tegelema
# Taken from:
# https://www.pyimagesearch.com/2017/02/06/faster-video-file-fps-with-cv2-videocapture-and-opencv/
class FileVideoStream:
    def __init__(self, path, queue_size=128):
        # Initialize the file video stream along with the boolean
        # Used to indicate if the thread should stop or not
        self.stream = cv2.VideoCapture(path)
        self.stream.set(cv2.CAP_PROP_BUFFERSIZE, 32)
        self.stopped = False

        self.FPS_MS = int( (1/30) * 1000 )

        # Initialize the queue used to store frames read from the video file
        # self.Q = Queue(maxsize=queue_size)
        self.frame = None

        # Start a thread to read frames from the file video stream
        self.thread = Thread(target=self.update)
        self.thread.daemon = True
        self.thread.start()

    def update(self):
        # Keep looping forever
        while True:
            # If thread indicator variable is set, stop the thread
            if self.stopped:
                return

            # Read the next frame from the file
            grabbed, self.frame = self.stream.read()

            # If grabbed == False, then video is at end
            if not grabbed:
                self.stop()
                return

    def read(self):
        # Return next frame from queue
        # frame = self.Q.get()
        cv2.imshow("Frame", self.frame)
        # Hoiab pilti korraks
        cv2.waitKey(self.FPS_MS)
        # return self.Q.get()

    def stop(self):
        # Indicate that the thread should be stopped
        self.stopped = True


def start_video_feed():
    # Start the file video stream thread and allow the buffer to start to fill
    print('[INFO] Starting video file thread...')
    # fvs = FileVideoStream('test_video.mp4').start()
    fvs = FileVideoStream('test_video.mp4')
    time.sleep(0.5)

    while True:
        try:
            fvs.read()
        except AttributeError:
            break

    # Cleanup
    cv2.destroyAllWindows()
    fvs.stop()
