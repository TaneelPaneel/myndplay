from threading import Thread
from mpeg import mpegCoder
from queue import Queue
import numpy
import time
import cv2

# Documentation from:
# https://cainmagi.github.io/FFmpeg-Encoder-Decoder-for-Python/

d = mpegCoder.MpegDecoder()
d.FFmpegSetup(b'test_video.mp4')        # Open source video file

Q = Queue(maxsize=256)

def update_queue():
    while True:
        p = d.ExtractGOP()              # Getting GOP (30 frames)

        if not p.any():
            return

        if not Q.qsize() >= 226:
            for frame in p:
                Q.put(frame)            # Putting all frames in GOP to queue

# t = Thread(target=update_queue)         # Starting thread for reading frames
# t.daemon = True
# t.start()
# time.sleep(5)
update_queue()
while Q.qsize() > 0:
    cv2.imshow("Frame", Q.get())
    cv2.waitKey(5)

d.clear()
