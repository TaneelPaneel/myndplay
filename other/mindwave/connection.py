from NeuroPy.NeuroPyFolder import NeuroPyFile

from time import sleep

"""
   Siin on kasutataud NeruoPy teeki https://github.com/lihas/NeuroPy
   Loob neuroPy objecti ehk objecti peapaela kohta
   Sisendiks tuleb anda stringina port ja int väärtusena lugemiskiirus (Baud rate),
   praegu panin default väärtused siia
"""
neuropy = NeuroPyFile.NeuroPyObject("COM4", 57600)


def attention_callback(attention_value):
    """this function will be called everytime NeuroPy has a new value for attention"""
    print("Value of attention is: ", attention_value)
    return None


"""See vist peaks kinni püüdma seda attention signaali, aga pole 100% kindel, dokumentatsioon oli puudulik"""
neuropy.setCallBack("attention", attention_callback)
neuropy.start()

try:
    while True:
        sleep(0.2)
finally:
    neuropy.stop()
