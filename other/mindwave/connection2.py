from myndplay import mindwave
import time

"""
    Siin on kasutatud mindwave-python teeki https://github.com/BarkleyUS/mindwave-python
    Siin luuakse headseti object ja antakse kaasa port, kus see headset on.
    NB! Teegi kirjelduses on kiras, et see teek on mõeldud peamiselt linux ja mac keskondade jaoks
    Peab kas proovima seda linuxi peal või anda pordiks kaasa COM5 vms 
"""

headset = mindwave.Headset('/dev/tty.MindWave')
time.sleep(2)

"""Connectib esimese saadaval oleva headsetiga"""
headset.autoconnect()

while headset.status != 'connected':
    time.sleep(0.5)
    if headset.status == 'standby':
        headset.connect()
        print("Retrying connect...")
print("Connected headset.")

while True:
    print("Attention 1: %s, Meditation 1: %s" % (headset.attention, headset.meditation))
