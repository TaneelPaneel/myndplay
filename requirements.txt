appdirs>=1.4.3
attrs>=19.3.0
black>=19.10b0
click>=7.1.1
cycler>=0.10.0
kiwisolver>=1.1.0
matplotlib>=3.2.1
numpy>=1.18.2
pathspec>=0.7.0
pyparsing>=2.4.6
PyQt5>=5.14.1
PyQt5-sip>=12.7.1
pyqtgraph>=0.10.0
pyserial>=3.4
python-dateutil>=2.8.1
regex>=2020.2.20
six>=1.14.0
toml>=0.10.0
typed-ast>=1.4.1
