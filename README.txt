# README #

LÕPUTÖÖ

Using Python 3.7.7

Use in terminal
python -m serial.tools.list_ports

Requirements:
pip install -r requirements.txt

Matplotlib's animation API:
https://scipy-cookbook.readthedocs.io/#head-3d51654b8306b1585664e7fe060a60fc76e5aa08
Maybe for realtime plotting

Backend:
backend\main.py

Frontend:
frontend\main.py

FOR PYINSTALLER (run in terminal):
pyinstaller --onefile --paths=venv/Lib/site-packages torp.spec torp.py
NB: Võibolla vaja absolute path site-packages puhul
